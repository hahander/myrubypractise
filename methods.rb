class Test
  def method_missing(method, *args)
    puts "No method: #{method}, but you don't cry #{args*3}"
  end
end

r = Test.new
r.red("sdasd", 45) => "No method: ff, but you don't cry ["sdasd", 45, "sdasd", 45, "sdasd", 45]"

class Test1
  def meth(x, y)
    puts "X: #{x}, Y: #{y}"
  end
  private
    def priv
      puts "I'm private"      
    end
end

r = Test1.new
r.send(:meth, 2, "asd") => "X: 2, Y: "asd""
r.priv => #No method exception(private access)#
r.send(:priv) => #Success#