begin #try
  # try open file
  f = File.open("ruby.txt")
  # .. continue file processing 
rescue ex => Exception #catch
  # .. handle errors, if any 
ensure #finally
  f.close unless f.nil?
  # always execute the code in ensure block 
end