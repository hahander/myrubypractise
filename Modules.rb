module MyModule
  def t1
    puts 'MyModule text'
  end
end

class TestModule
  extend MyModule
end

c = TestModule.new
c.t1 => #Exception#
TestModule.t1 => "MyModule text"

class TestModule
  include MyModule
end

c = TestModule.new
c.t1 => "MyModule text"
TestModule.t1 => #Exception#


# Rise exception any time, I try =)
module Wrong
  def self.t1
    puts 'MyModule'
  end
end

