class Test
  def self.hello
    puts 'self Hello'
  end

  def hello
    puts 'Hello'
  end
end

c = Test.new
c.hello => "Hello"
Test.hello => "self Hello"

#meta
class Test
  class << self
    def hello
      puts 'self hello'
    end
  end
end

class Dog

end

d = Dog.new

def d.name
  puts 'Doggy'
end

d.name => "Doggy"

class << d
  def run
    pust 'Can run'
  end
end

d.run => "Can run"
Dog.name => #Exception#
Dog.run => #Exception#
